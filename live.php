<?php
$JAGSReturn['meta'] = 'text/gemini';

// Get our live page and decode it
$livehtml = file_get_contents('https://node.radiobrony.fr/live.html');
$str = html_entity_decode($livehtml, ENT_QUOTES | ENT_HTML5 | ENT_XML1, 'UTF-8');

// Harsh conversion from HTML to Gemtext
// Main title
$str = preg_replace('/<h1>(.*?)<\/h1>/m', "\n# $1\n", $str);
// Promote <h2> and <h3> one rank higher
$str = preg_replace('/<h2>(.*?)<\/h2>/m', "\n# $1\n", $str);
$str = preg_replace('/<h3>(.*?)<\/h3>/m', "\n## $1\n", $str);
// Hebdo Pony's titles as <h3>
$str = preg_replace('/<img title="(.*)" alt.+hp\.radiobrony\.fr.+">/m', "\n### $1\n", $str);
// Convert links
$str = preg_replace('/<a.*?href="(.*?)".*?>(.*?)<\/a>/m', "\n=> $1 $2\n", $str);
// Convert iframe to links
$str = preg_replace('/<iframe.*src="(.*?)".*>/m', "\n=> $1\n", $str);
// Remove all HTML
$str = preg_replace('/<(.*?)>/ms', "", $str);
// Remove leading whitepsaces
$str = preg_replace('/(^ +)/m', "", $str);
?>
# Radio Brony - Live

> (Veuillez noter que cette page est pensée pour de l'HTML. Visitez notre site en HTTPS pour une meilleure expérience.)

<?php echo $str; ?>
