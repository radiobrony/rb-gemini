<?php
$JAGSReturn['meta'] = 'text/gemini';

// Now playing data
$npget = file_get_contents('https://radiobrony.fr/infos.json');
$np = json_decode($npget, true);

// Latest podcasts
$podget = file_get_contents('https://pod.radiobrony.fr/api/latest');
$pod = json_decode($podget, true);

// Next calendar events
$evget = file_get_contents('https://node.radiobrony.fr/events.json');
$ev = json_decode($evget, true);
?>
# Radio Brony

L'unique webradio française qui diffuse 24/7 de la musique et des émissions inspirées de la série télé MLP:FiM (sauf samedi soir, mais chut).

Actuellement à l'antenne :
> <?php echo $np['now_playing']['full']; ?>


=> /live Live
=> /podcasts Podcasts
=> /faq FAQ
=> /equipe Equipe


## Dernier podcast

<?php
$e = $pod['latest'][0]['episode'];
echo "=> " . $e['link'] . " " . $e['title'] . "\n";
echo "Présenté par " . $e['author'] . "\n";
echo "> " . $e['subtitle'] . "\n";
if ($e['season']) echo "Saison " . $e['season'] . " - Episode " . $e['episode'] . " - ";
echo "Durée : " . $e['duration'] . "s \n";
if ($e['recDate']) echo "Enregistré le " . date('d/m/Y \à H:i', strtotime($e['recDate'])) . "\n";
echo "Paru le " . date('d/m/Y \à H:i', strtotime($e['pubDate'])). "\n";
?>


## Prochaines émissions

<?php
foreach ($ev['events'] as $e) {
  if ($e['first'] && $e['start'] > time()) {
    echo "* " . date('d/m/Y \à H:i', $e['start']) . " : " . $e['title'] . "\n";
  }
}
?>


## Nous écouter partout

Nos flux :

=> https://radiobrony.live/mp3 MP3 (128kbps)
=> https://radiobrony.live/aac AAC (96kbps)
=> https://radiobrony.live/ogg Vorbis (~192kbps)
=> https://radiobrony.live/hls/live.m3u8 HLS (AAC adaptatif)

Services externe :

=> https://hoofsounds.little.my/ Extension Chrome/Firefox
=> https://tunein.com/radio/Radio-Brony-s185719/ TuneIn


## Nous suivre et nous rejoindre

=> https://twitter.com/RadioBrony Twitter
=> https://equestria.social/@RadioBrony Mastodon
=> https://discord.gg/MdhuDym Discord
=> https://www.last.fm/user/RadioBrony Last.fm
=> https://twitch.tv/radiobronyfr Twitch
=> https://www.youtube.com/@RadioBrony YouTube
