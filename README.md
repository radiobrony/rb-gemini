# Radio Brony on Gemini

This is the source code of <gemini://radiobrony.fr>

We're using [JAGS-PHP] as the Gemini server, with `index.php` set in the `default_index_file` array.  
All dynamic data is fetched from our APIs and public endpoints.

Page were adapted from our website as of early February 2023.

[JAGS-PHP]: https://codeberg.org/codeandcreate/JAGS-PHP
