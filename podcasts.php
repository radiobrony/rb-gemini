<?php
$JAGSReturn['meta'] = 'text/gemini';

$podget = file_get_contents('https://pod.radiobrony.fr/api/latest');
$pod = json_decode($podget, true);
?>
# Radio Brony - Podcasts

Liste des derniers épisodes.

<?php
foreach ($pod['latest'] as $p) {
  $s = $p['show'];
  $e = $p['episode'];

  // Remove tracker and link to MP3
  echo "=> " . str_replace('dts.podtrac.com/redirect.mp3/', '', $e['file']) . "\t";
  // Prepend title with the Y-m-d date to be detected as a feed
  echo date('Y-m-d', strtotime($e['pubDate'])) . " - " . $e['title'] . "\n";

  // Add infos
  echo $s['title'] . ", présenté par " . $e['author'] . "\n";
  echo "> " . $e['subtitle'] . "\n";
  if ($e['season']) echo "Saison " . $e['season'] . " - Episode " . $e['episode'] . " - ";
  echo "Durée : " . $e['duration'] . "s \n";
  if ($e['recDate']) echo "Enregistré le " . date('d/m/Y \à H:i', strtotime($e['recDate'])) . "\n";

  // Link to HTTPS site
  echo "=> " . $e['link'] . " Page complète\n\n";
}
?>
